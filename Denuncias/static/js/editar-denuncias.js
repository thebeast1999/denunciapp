$(document).ready(() => {
	$('.form-inline').hide();
	$('nav').addClass('navegacion-dark');

	$(() => {
		initMap();
		initMapModal();
	});

	$('#editar-ubicacion').click(e => {
		e.preventDefault();
		initMapModal();
		$('#modal-ubicacion').modal();
	});

	$('#buscador-coordenadas').keyup(e => {
		e.preventDefault();

		request = { query: e.currentTarget.value, fields: ['name', 'geometry'] };

		services.findPlaceFromQuery(request, function(results, status) {
			if (status === google.maps.places.PlacesServiceStatus.OK) {
				mapModal.setCenter(results[0].geometry.location);
			}
		});
	});
	$('#form-editar-denuncia').validate({
		rules: {
			'titulo-cambiar': {
				required: true,
				minlength: 5,
				number: false
			},
			'tipo-denuncia': {
				required: true
			},
			'coordenadas-cambiar': {
				required: true
			},
			'descripcion-denuncia': {
				required: true,
				minlength: 10,
				maxlength: 200
			}
		},
		messages: {
			'titulo-cambiar': {
				required: '(*) Este campo es obligatorio.',
				minlength: 'El titulo de la denuncia debe tener mas de 5 caracteres.',
				number: 'El titulo de la denuncia no puede tener números.'
			},
			'tipo-denuncia': {
				required: '(*) Este campo es obligatorio.'
			},
			'coordenadas-cambiar': {
				required: '(*) Este campo es obligatorio.'
			},
			'descripcion-denuncia': {
				required: '(*) Este campo es obligatorio.',
				minlength:
					'La descripción de la denuncia debe tener mas de 9 caracteres.',
				maxlength:
					'La descripción de la denuncia no puede tener mas de 200 caracteres.'
			}
		}
	});

	$('#cancelar-edicion-denuncia').click(e => {
		e.preventDefault();

		$(location).attr('href', '/denuncias/mis_denuncias');
	});
});

//Funciones para mapas
var mapEditar, mapModal;
var services;

//Mapa de visualización de solo lectura
function initMap() {
	let lat = parseFloat(
		$('#coordenadas-cambiar')
			.val()
			.split(', ')[0]
			.split('(')[1],
		10
	);
	let lng = parseFloat(
		$('#coordenadas-cambiar')
			.val()
			.split(', ')[1]
			.split(')')[0],
		10
	);

	console.log(lat);
	console.log(lng);

	mapEditar = new google.maps.Map(document.getElementById('mapa'), {
		center: { lat: lat, lng: lng },
		zoom: 17,
		zoomControl: false,
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: false,
		fullscreenControl: false
	});

	var marker = new google.maps.Marker({
		position: { lat: lat, lng: lng },
		map: mapEditar
	});
}

//Mapa modal para seleccionar nuevas coordenadas
function initMapModal() {
	//Ubicación inicial
	let lat = parseFloat(
		$('#coordenadas-cambiar')
			.val()
			.split(', ')[0]
			.split('(')[1],
		10
	);

	let lng = parseFloat(
		$('#coordenadas-cambiar')
			.val()
			.split(', ')[1]
			.split(')')[0],
		10
	);

	//Declaración de mapa
	mapModal = new google.maps.Map(document.getElementById('modal-mapa'), {
		center: { lat: lat, lng: lng },
		zoom: 17,
		zoomControl: false,
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: false,
		fullscreenControl: false
	});

	mapEditar.setCenter(new google.maps.LatLng(lat, lng));
	var marker = new google.maps.Marker({
		position: { lat: lat, lng: lng },
		map: mapModal
	});

	//Inicialización de servicios de búsqueda
	services = new google.maps.places.PlacesService(mapModal);

	//Evento de click sobre el mapa
	google.maps.event.addListener(mapModal, 'click', e => {
		let lat = e.latLng.lat();
		let lng = e.latLng.lng();

		$('#p-coordenadas').text(
			lat.toString().substring(0, 9) + ',  ' + lng.toString().substring(0, 9)
		);

		$('#coordenadas-cambiar').val(e.latLng);
		$.when($('#modal-ubicacion').modal('hide')).done(() => {
			initMap();
			mapEditar.setCenter(new google.maps.LatLng(lat, lng));
			marker = new google.maps.Marker({
				position: { lat: lat, lng: lng },
				map: mapEditar
			});
		});
	});
}
