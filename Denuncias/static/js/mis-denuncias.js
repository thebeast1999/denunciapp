$(document).ready(() => {
	$(() => {
		initMap();
	});

	$('.form-inline').hide();
	$('nav').addClass('navegacion-dark');

	//Masonry
	$('#contenedor-denuncia').masonry({
		itemSelector: '.denuncia'
	});

	//Ver
	$('.btn-ver').click(e => {
		var id = $(e.target)
			.attr('id')
			.split('-')[1];

		$('#titulo-ver').text($('#titulo-' + id).val());
		$('#tipo-ver').text($('#tipo-' + id).val());
		$('#fechahora-ver').text(
			$('#fecha-' + id).val() + ' - ' + $('#hora-' + id).val()
		);
		$('#descripcion-ver').text($('#descripcion-' + id).val());

		let lat = parseFloat(
			$('#coordenadas-' + id)
				.val()
				.split(', ')[0]
				.split('(')[1],
			10
		);

		let lng = parseFloat(
			$('#coordenadas-' + id)
				.val()
				.split(', ')[1]
				.split(')')[0],
			10
		);

		map.setCenter(new google.maps.LatLng(lat, lng));

		var marker = new google.maps.Marker({
			position: { lat: lat, lng: lng },
			map: map
		});

		$('#editarModal').attr('href', '/denuncias/editar_denuncias/' + id);
		$('.eliminarModal').attr('id', id);
		/*$('#eliminarModal').click(e => {
			e.preventDefault();
			$('#' + id).click();
		});*/
	});

	$('#modalInfo').on('hidden.bs.modal', e => {
		initMap();
	});

	//Eliminar
	$('.btn-eliminar, .eliminarModal').click(e => {
		e.preventDefault();

		$.sweetModal({
			title: '<b>Eliminar Denuncia</b>',
			width: '40vw',
			showCloseButton: true,
			blocking: true,
			content: '<b>¿Está seguro de que desea eliminar esta denuncia?</b>',
			theme: $.sweetModal.THEME_DARK,
			icon: $.sweetModal.ICON_WARNING,
			buttons: {
				cancelar: {
					label: 'Cancelar',
					classes: 'secondaryB',
					action: () => {
						return true;
					}
				},
				confirmar: {
					label: 'Eliminar',
					classes: 'redB',
					action: () => {
						var id_denuncia = $(e.target).attr('id');

						$.when(
							$.post('/denuncias/eliminar', { id_den: id_denuncia })
								.done(() => {
									$('#modalInfo').modal('hide');
									$.sweetModal({
										content: 'Denuncia eliminada correctamente.',
										theme: $.sweetModal.THEME_DARK,
										showCloseButton: false,
										width: '40vw',
										blocking: true,
										icon: $.sweetModal.ICON_SUCCESS,
										timeout: 1500,
										onClose: () => {
											$.when($('#div-' + id_denuncia).fadeOut()).done(() => {
												$('#div-' + id_denuncia).remove();
												$('#contenedor-denuncia').masonry({
													itemSelector: '.denuncia',
													columnWidth: '.grid-sizer',
													percentPosition: true
												});
											});
										}
									});
								})
								.fail(() => {
									$.sweetModal({
										content: 'Error al eliminar denuncia.',
										theme: $.sweetModal.THEME_DARK,
										showCloseButton: false,
										width: '40vw',
										blocking: true,
										icon: $.sweetModal.ICON_ERROR,
										timeout: 1500
									});
								})
						);
					}
				}
			}
		});
	});

	//Busqueda
	$('#busqueda').keyup(e => {
		e.preventDefault();

		//Obtener valor del input
		filtro = $('#busqueda')
			.val()
			.toUpperCase();

		//Por cada tarjeta
		$('.denuncia').each((index, element) => {
			//Obtener datos a comparar
			titulo = $(element)
				.find('.card .card-header h2.card-title')
				.text()
				.toUpperCase();

			tipo = $(element)
				.find('.card .card-header h5.sub-tipo')
				.text()
				.toUpperCase();

			fecha = $(element)
				.find('.card .card-header h5.sub-fecha')
				.text()
				.toUpperCase();

			descripcion = $(element)
				.find('.card .card-body p.card-text')
				.text()
				.toUpperCase();

			condicion =
				~titulo.indexOf(filtro) ||
				~tipo.indexOf(filtro) ||
				~fecha.indexOf(filtro) ||
				~descripcion.indexOf(filtro);

			if (condicion) {
				$.when($(element).fadeIn()).done(() => {
					$('#contenedor-denuncia').masonry({
						itemSelector: '.denuncia',
						columnWidth: '.col-lg-4',
						percentPosition: true
					});
				});

				$('#contenedor-denuncia').masonry({
					itemSelector: '.denuncia',
					columnWidth: '.grid-sizer',
					percentPosition: true
				});
			} else {
				$.when($(element).fadeOut()).done(() => {
					$('#contenedor-denuncia').masonry({
						itemSelector: '.denuncia',
						columnWidth: '.col-lg-4',
						percentPosition: true
					});
				});
				$('#contenedor-denuncia').masonry({
					itemSelector: '.denuncia',
					columnWidth: '.grid-sizer',
					percentPosition: true
				});
			}
		});
	});
});

//Funciones para mapas
var map;

//Mapa modal para seleccionar nuevas coordenadas
function initMap() {
	//Declaración de mapa
	map = new google.maps.Map(document.getElementById('map'), {
		center: { lat: 15, lng: 15 },
		zoom: 17,
		zoomControl: false,
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: false,
		fullscreenControl: false
	});
}
