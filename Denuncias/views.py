from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from Denuncias.models import Denuncia, TIPOS
from Cuentas.models import Perfil_Usuario
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

# Create your views here.


@login_required
def registrar(request):
    if request.method == 'POST':

        user = request.user
        denuncia = Denuncia()
        denuncia.titulo = request.POST.get('titulo-denuncia')
        denuncia.coordenadas = request.POST.get('coordenadas-denuncia')
        denuncia.tipo = request.POST.get('tipo-denuncia')
        denuncia.descripcion = request.POST.get('descripcion-denuncia')
        denuncia.autor = user

        try:
            denuncia.create()
        except:
            return False

        return HttpResponse('Registro correcto')


@login_required
@csrf_exempt
def eliminar(request):
    if request.method == 'POST':
        id = request.POST.get('id_den')
        denuncia = Denuncia()
        denuncia.deletex(id)

        return HttpResponseRedirect('/denuncias/mis_denuncias')


@login_required
def ver_todas(request):
    usuario = request.user
    perfilUsuario = Perfil_Usuario()
    perfilUsuario.read(usuario)

    lista_denuncias = Denuncia().read_all(usuario)

    return render(request, 'Denuncias/mis-denuncias.html', {'denuncias': lista_denuncias, 'avatar': perfilUsuario.avatar_image})


@login_required
def editar(request, id):
    usuario = request.user
    perfil = Perfil_Usuario()
    perfil.read(usuario)

    denuncia = Denuncia()
    denuncia.read(id)

    coordenada_corta = denuncia.coordenadas.split(',')[0].split(
        '(')[1][0:9] + ",  " + denuncia.coordenadas.split(',')[1].split(')')[0][0:9]

    if usuario == denuncia.autor:
        return render(request, 'Denuncias/editar-denuncias.html', {'avatar': perfil.avatar_image, 'tipos': TIPOS, 'denuncia': denuncia, 'coordenada_corta': coordenada_corta})
    else:
        return HttpResponse("Solo puedes editar denuncias de tu autoria")


def editar_denuncias(request):
    if request.method == 'POST':
        id = request.POST.get('id_denuncia')
        denuncia = Denuncia()
        denuncia.read(id)

        denuncia.titulo = request.POST.get('titulo-cambiar')
        denuncia.tipo = request.POST.get('tipo-denuncia')
        denuncia.coordenadas = request.POST.get('coordenadas-cambiar')
        denuncia.descripcion = request.POST.get('descripcion-denuncia')

        denuncia.update()

        return HttpResponseRedirect('/denuncias/mis_denuncias')
