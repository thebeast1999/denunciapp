from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import date
import os


# Create your models here.

TIPOS = (
    ('Robo', 'ROBO'),
    ('Incendio', 'INCENDIO'),
    ('Choque', 'CHOQUE'),
    ('Abuso de Poder', 'ABUSO_PODER'),
    ('Disturbios', 'DISTURBIOS'),
    ('Asesinato', 'ASESINATO'),
    ('Accidente', 'ACCIDENTE'),
    ('Trabajos', 'TRABAJOS'),
    ('Otros', 'OTROS')
)


class Denuncia(models.Model):
    # Atributos
    id = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=20, blank=False, null=False)
    descripcion = models.TextField(max_length=70, blank=False, null=False)
    fecha_creacion = models.DateField(blank=True, null=True)
    hora_creacion = models.TimeField(blank=True, null=True)
    tipo = models.CharField(max_length=20, choices=TIPOS, default='CHILE')
    coordenadas = models.TextField(max_length=50)
    autor = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True)

    # Metodos
    def __str__(self):
        return self.titulo

    def create(self):
        self.fecha_creacion = date.today()
        self.hora_creacion = timezone.now()

        try:
            self.save()
            return True
        except:
            return False

    def read(self, id_denuncia):
        denuncia = Denuncia.objects.get(id=id_denuncia)

        self.id = denuncia.id
        self.titulo = denuncia.titulo
        self.descripcion = denuncia.descripcion
        self.fecha_creacion = denuncia.fecha_creacion
        self.hora_creacion = denuncia.hora_creacion
        self.tipo = denuncia.tipo
        self.coordenadas = denuncia.coordenadas
        self.autor = denuncia.autor

    def read_all(self, user):
        lista_denuncias = Denuncia.objects.filter(
            autor=user).order_by("-fecha_creacion", "hora_creacion")

        return lista_denuncias

    def update(self):
        try:
            self.save()
            return True
        except:
            return False

    def deletex(self, id_denuncia):
        try:
            self.read(id_denuncia)
            self.delete()
            return True
        except:
            return False
