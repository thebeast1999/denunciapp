import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestDenuncias:

    def test_model(self):
        obj = mixer.blend('Denuncias.Denuncia')
        assert obj.pk == 1, 'Mensaje de error?'

    def test_str(self):
        titulo = "Titulo Prueba"
        obj = mixer.blend('Denuncias.Denuncia', titulo=titulo)

        assert str(obj) == titulo

    def test_create(self):
        obj = mixer.blend('Denuncias.Denuncia')
        assert obj.create() == True

    def test_read(self):
        obj = mixer.blend('Denuncias.Denuncia')
        obj.create()
        obj2 = mixer.blend('Denuncias.Denuncia')
        obj2.read(obj.id)

        assert obj2 == obj

    def test_update(self):
        titulo_modificado = 'Titulo modificado'
        obj = mixer.blend('Denuncias.Denuncia')

        obj.titulo = titulo_modificado

        assert obj.update() == True

    def test_delete(self):
        obj = mixer.blend('Denuncias.Denuncia')

        assert obj.deletex(obj.id)
