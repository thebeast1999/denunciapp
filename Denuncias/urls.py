from . import views
from django.urls import path, include

urlpatterns = [
    path("registrar", views.registrar, name="registrar"),
    path("eliminar", views.eliminar, name="eliminar"),
    path("mis_denuncias", views.ver_todas, name="mis_denuncias"),
    path("editar_denuncias/<int:id>/", views.editar, name="editar"),
    path("do_editar", views.editar_denuncias, name="editar_denuncias")
]
