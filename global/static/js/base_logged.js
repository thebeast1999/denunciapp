$(document).ready(function() {
	$('.boton, #perfil').tooltipster({
		theme: 'tooltipster-punk',
		trigger: 'hover'
	});

	$(() => {
		$('[data-toggle="popover"]').popover({
			html: true,
			content: function() {
				return $('#popover-content').html();
			}
		});
	});

	$('[data-toggle="popover"]').on('show.bs.popover', () => {
		$('#perfil').tooltipster('disable');
	});

	$('[data-toggle="popover"]').on('hidden.bs.popover', () => {
		$('#perfil').tooltipster('enable');
	});

	$('#homeButton').click(e => {
		e.preventDefault();

		$(location).attr('href', '/home');
	});

	$('#denunciasButton').click(e => {
		e.preventDefault();

		$(location).attr('href', '/denuncias/mis_denuncias');
	});
});
