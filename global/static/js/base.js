$(document).ready(() => {
	//Login Modal
	var login_click = false;

	$('.login').click(() => {
		if (!login_click) {
			login_click = true;
			$.sweetModal({
				title: '<b>Iniciar Sesión</b>',
				width: '40vw',
				showCloseButton: true,
				blocking: false,
				theme: $.sweetModal.THEME_DARK,
				content:
					'<form method="POST" action="/cuentas/autenticar" id="inicio-sesion"><div class="form-group"><label for="email"><b>Correo Electrónico</b></label><input name="email" id="email" type="email" class="form-control" placeholder="ejemplo@correo.com"/></div><div class="form-group"><label for="contrasena"><b>Contraseña</b></label><input name="contrasena" id="contrasena" class="form-control" type="password" placeholder="Contraseña" minlength="8" /></div></form>',
				buttons: {
					cancelar: {
						label: 'Cancelar',
						classes: 'secondaryB',
						action: () => {
							login_click = false;
						}
					},
					iniciarSesion: {
						label: 'Iniciar Sesión',
						action: () => {
							//Acciones para iniciar sesion
							$.post('/cuentas/autenticar', $('#inicio-sesion').serialize())
								.done(() => {
									$.sweetModal({
										content: 'Iniciando Sesión',
										theme: $.sweetModal.THEME_DARK,
										showCloseButton: false,
										width: '40vw',
										blocking: true,
										icon: $.sweetModal.ICON_SUCCESS,
										timeout: 1500,
										onClose: () => {
											location.reload();
											return true;
										}
									});
								})
								.fail(() => {
									$.sweetModal({
										content: 'Usuario o contraseña incorrectos',
										theme: $.sweetModal.THEME_DARK,
										showCloseButton: false,
										width: '40vw',
										blocking: true,
										icon: $.sweetModal.ICON_ERROR,
										timeout: 1500,
										onClose: () => {
											login_click = false;
											$('.login').trigger('click');
										}
									});
								});
						}
					}
				},
				onClose: () => {
					login_click = false;
				}
			});
		}
	});

	//Colapsar menu de la navbar movil tras hacer click en una opción
	$('.navbar-nav>li>a').on('click', function() {
		$('.navbar-collapse').collapse('hide');
	});
});
