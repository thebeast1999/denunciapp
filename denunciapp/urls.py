from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url

urlpatterns = [
    url('^api/v1/',include('social_django.urls', namespace='social')),
    path('admin/', admin.site.urls),
    path("", include('Index.urls')),
    path("home/", include('App.urls')),
    path("cuentas/", include('Cuentas.urls')),
    path("denuncias/", include('Denuncias.urls'))
]
