$(document).ready(() => {
	//Animar scroll a sección de información
	$('#Mas-Info, #Mas-Info-Mob').click(() => {
		$('section').animatescroll({ scrollSpeed: 1000, easing: 'easeInOutQuad' });
	});

	//Botón Registro
	$('.registro').click(function(e) {
		e.preventDefault();
		$(location).attr('href', 'registro');
	});

	//Animar vuelta a landing page
	$('.navbar-brand, #BackTop').click(() => {
		$('body').animatescroll({ scrollSpeed: 1000, easing: 'easeInOutQuad' });
	});

	//Tooltip
	$('.fab, .fas').tooltipster({
		theme: 'tooltipster-punk',
		trigger: 'custom',
		triggerOpen: {
			mouseenter: true,
			tap: true // For touch device
		},
		triggerClose: {
			mouseleave: true, // For mouse
			tap: true // For touch device
		}
	});

	//Solidificar Navbar en la sección de información
	$(window).scroll(() => {
		if ($(document).scrollTop() >= $(window).height() - 100) {
			$('nav').addClass('navegacion-dark');
		} else {
			$('nav').removeClass('navegacion-dark');
		}
	});

	//Media Querys en JQuery
	if (
		$('header').width() <= 576 ||
		(window.matchMedia('(orientation: landscape)').matches &&
			$('header').width() <= 768)
	) {
		$('body').addClass('activar-scroll');
	} else {
		$('body').removeClass('activar-scroll');
	}
});
