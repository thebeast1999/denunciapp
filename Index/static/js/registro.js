$(document).ready(() => {
	$('.navbar').addClass('navegacion-dark');
	$('#Mas-Info-Mob').hide();

	$('.navbar-brand').click(e => {
		e.preventDefault();
		$(location).attr('href', '/');
	});

	$('.registro-self').click(e => {
		e.preventDefault();
		$('body').animatescroll({ scrollSpeed: 1000, easing: 'easeOutBounce' });
	});

	$('#formulario').validate({
		rules: {
			nombres: {
				required: true,
				minlength: 2,
				number: false
			},
			contraseña: {
				required: true,
				minlength: 8
			},
			contraseña2: {
				required: true,
				minlength: 8,
				equalTo: '#contraseña'
			},
			apellidos: {
				required: true,
				minlength: 2,
				number: false
			},
			email: {
				required: true,
				email: true
			},
			fecha: {
				required: true
			},
			pais: {
				required: true
			},
			telefono: {
				required: true,
				number: true,
				digits: true,
				minlength: 9,
				maxlength: 9
			},
			check: {
				required: true
			}
		},
		messages: {
			nombres: {
				required: '(*) Este campo es obligatorio',
				minlength: 'El nombre debe tener mas de 1 caracter.',
				number: 'Un nombre no puede tener números.'
			},
			contraseña: {
				required: '(*) Este campo es obligatorio',
				minlength: 'La contraseña debe tener al menos 8 caracteres.'
			},
			contraseña2: {
				required: '(*) Este campo es obligatorio',
				minlength: 'La contraseña debe tener al menos 8 caracteres.',
				equalTo: 'Las contraseñas ingresadas no coinciden.'
			},
			apellidos: {
				required: '(*) Este campo es obligatorio',
				minlength: 'El nombre debe tener mas de 1 caracter.',
				number: 'Un nombre no puede tener números.'
			},
			email: {
				required: '(*) Este campo es obligatorio',
				email: 'Ingresa un email válido.'
			},
			fecha: {
				required: '(*) Este campo es obligatorio'
			},
			pais: {
				required: '(*) Este campo es obligatorio'
			},
			telefono: {
				required: '(*) Este campo es obligatorio',
				number: 'Solo puedes ingresar números.',
				digits: 'Solo puedes ingresar números enteros.',
				minlength: 'El telefono debe tener 9 dígitos.',
				maxlength: 'El telefono debe tener 9 dígitos.'
			},
			check: {
				required: '(*) Este campo es obligatorio'
			}
		}
	});

	$('#miboton').click(e => {
		e.preventDefault();

		if ($('#formulario').valid()) {
			$.sweetModal({
				content: '¡Has sido registrado correctamente!',
				theme: $.sweetModal.THEME_DARK,
				showCloseButton: false,
				width: '40vw',
				blocking: true,
				icon: $.sweetModal.ICON_SUCCESS,
				timeout: 1500,
				onClose: () => {
					$('#formulario').submit();
				}
			});
		}
	});
});
