from django.shortcuts import render
from django.http import HttpResponseRedirect
from Cuentas.models import PAISES

# Create your views here.


def index(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/home')
    else:
        return render(request, 'Index/index.html', {})


def formulario_registro(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/home')
    else:

        return render(request, 'Index/registro.html', {'paises': PAISES})
