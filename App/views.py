from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from Cuentas.models import Perfil_Usuario
from Denuncias.models import TIPOS
from django.http import HttpResponseRedirect, HttpResponse

@login_required
def home(request):
    usuario = request.user
    perfilUsuario = Perfil_Usuario()
    perfilUsuario.read(usuario)

    datos = {'avatar': perfilUsuario.avatar_image, 'tipos': TIPOS}

    return render(request, 'App/home.html', datos)


