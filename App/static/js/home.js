$(document).ready(() => {
	//Inicializar Mapa
	$(() => {
		initMap();
	});

	//Encontrar mi ubicación
	$('#localizar').click(() => {
		$(() => {
			localizar();
			$('#busqueda-nave, #busqueda-flot').val('');
		});
	});

	//Búsqueda en el mapa
	$('#busqueda-nave, #busqueda-flot').keyup(e => {
		e.preventDefault();

		request = { query: e.currentTarget.value, fields: ['name', 'geometry'] };

		services.findPlaceFromQuery(request, function(results, status) {
			if (status === google.maps.places.PlacesServiceStatus.OK) {
				map.setCenter(results[0].geometry.location);
			}
		});
	});

	//POST segundo plano
	$('#btn-publicar').click(e => {
		e.preventDefault();

		$.post('/denuncias/registrar', $('#form-home').serialize())
			.done(() => {
				$('#nueva-denuncia').modal('hide');
				$.sweetModal({
					content: 'Denuncia agregada correctamente.',
					icon: $.sweetModal.ICON_SUCCESS,
					theme: $.sweetModal.THEME_DARK,
					timeout: 1000,
					showCloseButton: false
				});
			})
			.fail(() => {
				$('#nueva-denuncia').modal('hide');
				$.sweetModal({
					content: 'Error al crear denuncia.',
					icon: $.sweetModal.ICON_ERROR,
					theme: $.sweetModal.THEME_DARK,
					timeout: 1000,
					showCloseButton: false
				});
			});
	});

	$('#nueva-denuncia').on('hidden.bs.modal', e => {
		$('#form-home')[0].reset();
	});

	//Validación formulario
	$('#form-home').validate({
		rules: {
			'titulo-denuncia': {
				required: true,
				minlength: 5,
				number: false
			},
			'tipo-denuncia': {
				required: true
			},
			'coordenadas-denuncia': {
				required: true
			},
			'descripcion-denuncia': {
				required: true,
				minlength: 10,
				maxlength: 200
			}
		},
		messages: {
			'titulo-denuncia': {
				required: '(*) Este campo es obligatorio.',
				minlength: 'El titulo de la denuncia debe tener mas de 5 caracteres.',
				number: 'El titulo de la denuncia no puede tener números.'
			},
			'tipo-denuncia': {
				required: '(*) Este campo es obligatorio.'
			},
			'coordenadas-denuncia': {
				required: '(*) Este campo es obligatorio.'
			},
			'descripcion-denuncia': {
				required: '(*) Este campo es obligatorio.',
				minlength:
					'La descripción de la denuncia debe tener mas de 9 caracteres.',
				maxlength:
					'La descripción de la denuncia no puede tener mas de 200 caracteres.'
			}
		}
	});
});

//Funciones para mapas
var map;
var services;

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: { lat: -34.397, lng: 150.644 },
		zoom: 17,
		zoomControl: false,
		mapTypeControl: false,
		scaleControl: false,
		streetViewControl: false,
		fullscreenControl: false
	});

	services = new google.maps.places.PlacesService(map);

	google.maps.event.addListener(map, 'click', e => {
		$('#p-coordenadas').text(
			e.latLng
				.lat()
				.toString()
				.substring(0, 10) +
				' ' +
				e.latLng
					.lng()
					.toString()
					.substring(0, 10)
		);
		$('#coordenadas').val(e.latLng);

		$('#nueva-denuncia').modal();
	});

	localizar();
}

const localizar = () => {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(
			function(position) {
				var pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				map.setCenter(pos);
			},
			function() {
				handleLocationError(true, infoWindow, map.getCenter());
			},
			{ timeout: 30000, enableHighAccuracy: true, maximumAge: 75000 }
		);
	} else {
		handleLocationError(false, infoWindow, map.getCenter());
	}
};
