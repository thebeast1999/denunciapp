from . import views
from django.urls import path, include

urlpatterns = [
    path("registrar", views.registrar_usuario, name="registrar"),
    path("autenticar", views.auth, name="autenticar"),
    path("perfil", views.perfil, name="perfil"),
    path("perfil/editar", views.editar, name="editar-perfil"),
    path("editar", views.editar_perfil, name="funcion-editar"),
    path("contraseña/editar", views.cambiar_contraseña, name="funcion-editar-contraseña"),
    path("logout", views.cerrar_sesion, name="cerrar-sesion"),
    path("error" , views.error, name="error")
]
