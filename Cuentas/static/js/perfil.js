$(document).ready(() => {
	$('.form-inline').hide();

	$('nav').addClass('navegacion-dark');

	$('#btn-editar-perfil').click(function(e) {
		e.preventDefault();

		$(location).attr('href', 'perfil/editar');
	});

	$('#cambiar-contra').click(function(e) {
		e.preventDefault();

		$.post(
			'/cuentas/contraseña/editar',
			$('#form-editar-contraseña').serialize()
		)
			.done(() => {
				$.sweetModal({
					content: 'Contraseña editada correctamebnte',
					theme: $.sweetModal.THEME_DARK,
					showCloseButton: false,
					width: '40vw',
					blocking: true,
					icon: $.sweetModal.ICON_SUCCESS,
					timeout: 1500,
					onClose: () => {
						location.reload();
						return true;
					}
				});
			})
			.fail(() => {
				$.sweetModal({
					content: 'Su contraseña actual no coincide con la ingresada.',
					theme: $.sweetModal.THEME_DARK,
					showCloseButton: false,
					width: '40vw',
					blocking: true,
					icon: $.sweetModal.ICON_ERROR,
					timeout: 1500
				});
			});
	});

	$('#form-editar-contraseña').validate({
		rules: {
			'contrasena-editar': {
				required: true,
				minlength: 8
			},
			'contrasena2-editar': {
				required: true,
				minlength: 8,
				equalTo: '#contrasena-editar'
			}
		},
		messages: {
			'contrasena-editar': {
				required: '(*) Este campo es obligatorio',
				minlength: 'La contraseña debe tener al menos 8 caracteres.'
			},
			'contrasena2-editar': {
				required: '(*) Este campo es obligatorio',
				minlength: 'La contraseña debe tener al menos 8 caracteres.',
				equalTo: 'Las contraseñas ingresadas no coinciden.'
			}
		}
	});
});
