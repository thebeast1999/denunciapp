$(document).ready(() => {
	$('.form-inline').hide();

	$('nav').addClass('navegacion-dark');

	$('.foto-usuario').click(e => {
		e.preventDefault();
		$('#btn-files').click();
	});

	$('#files').change(function() {
		var input = this;
		var url = $(this).val();
		var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
		if (
			input.files &&
			input.files[0] &&
			(ext == 'gif' || ext == 'png' || ext == 'jpeg' || ext == 'jpg')
		) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('.foto-usuario').attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		} else {
			$('#img').attr('src', '/assets/no_preview.png');
		}
	});

	$('#cancelar-edicion').click(e => {
		e.preventDefault();

		$(location).attr('href', '/cuentas/perfil');
	});

	$('#form-editar').validate({
		rules: {
			'nombre-cambiar': {
				required: true,
				minlength: 2,
				number: false
			},
			'apellido-cambiar': {
				required: true,
				minlength: 2,
				number: false
			},
			'email-cambiar': {
				required: true,
				email: true
			},
			'fecha-nacimiento-cambiar': {
				required: true,
			},
			'pais-cambiar': {
				required: true,
			},
			'telefono-cambiar': {
				required: true,
				number: true,
				digits: true,
				minlength: 9,
				maxlength: 9
			}
		},
		messages: {
			'nombre-cambiar': {
				required : '(*) Este campo es obligatorio.',
				minlength: 'El nombre debe tener mas de 1 caracter.',
				number: 'Un nombre no puede tener números.'
			},
			'apellido-cambiar': {
				required : '(*) Este campo es obligatorio.',
				minlength: 'El nombre debe tener mas de 1 caracter.',
				number: 'Un nombre no puede tener números.'
			},
			'fecha-nacimiento-cambiar': {
				required : '(*) Este campo es obligatorio.'
			},
			'pais-cambiar': {
				required : '(*) Este campo es obligatorio.'
			},
			'email-cambiar': {
				required : '(*) Este campo es obligatorio.',
				email: 'Ingresa un email válido.'
			},
			'telefono-cambiar': {
				required : '(*) Este campo es obligatorio.',
				number: 'Solo puedes ingresar números.',
				digits: 'Solo puedes ingresar números enteros.',
				minlength: 'El telefono debe tener 9 dígitos.',
				maxlength: 'El telefono debe tener 9 dígitos.'
			}
		}
	});
});
