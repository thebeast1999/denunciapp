import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestPerfilUsuario:

    def test_model(self):
        obj = mixer.blend('Cuentas.Perfil_Usuario')
        assert obj.pk == 1, 'Mensaje de error?'

    def test_str(self):
        nombre = "Nombre Prueba"
        obj = mixer.blend('Cuentas.Perfil_Usuario', nombre=nombre)

        assert str(obj) == nombre

    def test_create(self):
        obj = mixer.blend('Cuentas.Perfil_Usuario')
        assert obj.create(obj.User) == True

    def test_read(self):
        obj = mixer.blend('Cuentas.Perfil_Usuario')
        obj.create(obj.User)
        obj2 = mixer.blend('Cuentas.Perfil_Usuario')
        obj2.read(obj.User)

        assert obj2 == obj

    def test_update(self):
        nombre_modificado = 'Nombre modificado'
        obj = mixer.blend('Cuentas.Perfil_Usuario')

        obj.nombre = nombre_modificado

        assert obj.update() == True
