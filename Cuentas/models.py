from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
import os

# Create your models here.

PAISES = (
    ('Argentina', 'ARGENTINA'),
    ('Bolivia', 'BOLIVIA'),
    ('Chile', 'CHILE'),
    ('Colombia', 'COLOMBIA'),
    ('Ecuador', 'ECUADOR'),
    ('Paraguay', 'PARAGUAY'),
    ('Peru', 'PERU'),
    ('Uruguay', 'URUGUAY'),
    ('Venezuela', 'VENEZUELA')
)


def obtener_ruta_imagen(instance, filename):
    return os.path.join('static/avatares', str(instance.User.id), str(instance.User.id)+"-avatar")


class Perfil_Usuario(models.Model):
    User = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    telefono = models.IntegerField()
    fecha_nacimiento = models.DateField()
    pais = models.CharField(max_length=9, choices=PAISES, default='CHILE')
    avatar_image = models.FileField(
        upload_to=obtener_ruta_imagen, blank=True, null=True)

    def __str__(self):
        return self.nombre

    def create(self, user):
        self.avatar_image = None
        self.User = user

        try:
            self.save()
            return True
        except:
            return False

    def read(self, user):
        perfil_usuario = Perfil_Usuario.objects.get(User=user)
        self.User = perfil_usuario.User
        self.nombre = perfil_usuario.nombre
        self.apellido = perfil_usuario.apellido
        self.telefono = perfil_usuario.telefono
        self.fecha_nacimiento = perfil_usuario.fecha_nacimiento
        self.pais = perfil_usuario.pais
        self.avatar_image = perfil_usuario.avatar_image

    def obtener_diccionario(self, user):
        perfilUsuario = Perfil_Usuario()
        perfilUsuario.read(user)

        fecha = str(user.date_joined).split(' ')[0].strip().split('-')[::-1]
        fecha = fecha[0] + '-' + fecha[1] + '-' + fecha[2]

        diccionario_perfil = {'nombre': perfilUsuario.nombre, 'apellido': perfilUsuario.apellido, 'telefono': perfilUsuario.telefono,
                              'email': user.email, 'fecha_nacimiento': perfilUsuario.fecha_nacimiento, 'pais': perfilUsuario.pais,
                              'fecha_registro': fecha, 'avatar': perfilUsuario.avatar_image}

        return diccionario_perfil

    def update(self):
        try:
            self.save()
            return True
        except:
            return False
