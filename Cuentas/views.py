from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.models import User
from Cuentas.models import Perfil_Usuario, PAISES
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm


# Create your views here.


def registrar_usuario(request):
    if request.method == 'POST':
        user = User.objects.create_user(request.POST.get(
            'email'), request.POST.get('email'), request.POST.get('contraseña'))

        perfil = Perfil_Usuario()

        perfil.nombre = request.POST.get('nombres')
        perfil.apellido = request.POST.get('apellidos')
        perfil.telefono = request.POST.get('telefono')
        perfil.fecha_nacimiento = request.POST.get('fecha')
        perfil.pais = request.POST.get('pais')

        perfil.create(user)

        return(HttpResponseRedirect("/"))


@csrf_exempt
def auth(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        contrasena = request.POST.get('contrasena')
        user = authenticate(username=email, password=contrasena)

        if user is not None:
            if user.is_active:
                request.session.set_expiry(86400)
                login(request, user)

                return HttpResponse('Inicio correcto')

            else:
                return False
        else:
            return False


@login_required
def perfil(request):

    perfil = Perfil_Usuario()
    diccionario_perfil = perfil.obtener_diccionario(request.user)

    return render(request, 'Cuentas/perfil.html', diccionario_perfil)


@login_required
def cerrar_sesion(request):
    logout(request)
    return(HttpResponseRedirect('/'))


@login_required
def editar_perfil(request):
    if request.method == 'POST':
        # Variables
        usuario = request.user
        perfilUsuario = Perfil_Usuario()
        perfilUsuario.read(usuario)

        # Editar usuario
        usuario.username = request.POST.get('email-cambiar')
        usuario.email = request.POST.get('email-cambiar')
        usuario.save()

        # Editar perfil usuario
        perfilUsuario.nombre = request.POST.get('nombre-cambiar')
        perfilUsuario.apellido = request.POST.get('apellido-cambiar')
        perfilUsuario.telefono = request.POST.get('telefono-cambiar')
        perfilUsuario.fecha_nacimiento = request.POST.get(
            'fecha-nacimiento-cambiar')
        perfilUsuario.pais = request.POST.get('pais-cambiar')

        # Editar imagen
        if request.FILES.get('imagen'):
            file = request.FILES.get('imagen')
            perfilUsuario.avatar_image = file

        perfilUsuario.update()

        return(HttpResponseRedirect('/cuentas/perfil'))


@login_required
def editar(request):
    perfil = Perfil_Usuario()

    diccionario_perfil = perfil.obtener_diccionario(request.user)
    diccionario_perfil['fecha_nacimiento'] = str(
        diccionario_perfil['fecha_nacimiento'])

    diccionario_perfil['paises'] = PAISES

    return render(request, 'Cuentas/editar-perfil.html', diccionario_perfil)


@login_required
def cambiar_contraseña(request):
    if request.method == 'POST':
        usuario = request.user

        if usuario.check_password(request.POST.get('contrasena-actual')):
            usuario.set_password(request.POST.get('contrasena-editar'))
            usuario.save()

            update_session_auth_hash(request, usuario)

            return HttpResponse('Cambio correcto')
        else:
            return False


def error(request):
    return render(request, 'Cuentas/error_404.html')
